from Tkinter import *
import tkFileDialog
from PIL import Image, ImageTk
import cv2
import numpy as np
import json
import inspect, os,fnmatch
import platform

c = 8
ro = 6

class App:

    def __init__(self, master):
        frame = Frame(master)
        frame.grid()

        # Button to choose an image
        self.imagebutton = Button(master, text="Load an Image", command=lambda:[f() for f in [self.open_image, self.clean_position, self.resize_window]])
        self.imagebutton.config(height=180, width=360)
        self.imagebutton.grid(row=0, column=0, columnspan=c, rowspan=ro)

        self.filedefault = None

        ###############################################################################

        # Label Network
        self.l1 = Label(master, text="Network")
        self.l1.grid(row=0, column=c,sticky=E)
        
        OSystem = platform.system() # get the Operating system

        # Check operating system to adjust the output file name
        if (OSystem == 'Linux'):
            outputfile = '../output'
        else:
            outputfile = '..\output'  

        OPTIONS = ['Test']
        for root, dirs, files in os.walk(outputfile):
            for file in fnmatch.filter(files, '*.meta'): # find all network options
                OPTIONS.append(file.split('.meta')[0])

        self.variable = StringVar(master)
        self.variable.set(OPTIONS[0]) # default value
        self.network = apply(OptionMenu, (master, self.variable) + tuple(OPTIONS))
        self.network.grid(row=0, column=c+1, sticky=W)

        # Label Prediction
        self.prediction = Label(master, text="Prediction")
        self.prediction.grid(row=1, column=c, sticky=E)

        # Choose prediction value
        self.predictionValue = Entry(master, width=6)
        self.predictionValue.grid(row=1, column=c+1,sticky=W)

        self.predictionValue.delete(0, END)
        self.predictionValue.insert(0, "6000") # default value
        
        # Label Keep
        self.keep = Label(master, text="Keep")
        self.keep.grid(row=2, column=c, sticky=E)

        # Choose Keep value
        self.keepValue = Entry(master, width=6)
        self.keepValue.grid(row=2, column=c+1, sticky=W)

        self.keepValue.delete(0, END)
        self.keepValue.insert(0, "300") # default value

        # Label Threshold
        self.threshold = Label(master, text="Threshold")
        self.threshold.grid(row=3, column=c, sticky=E)

        # Choose threshold value (with slider)
        self.thresholdValue = Scale(master, from_=0.08, to=1.0, resolution=0.05, orient=HORIZONTAL)
        self.thresholdValue.set(0.95) # default value
        self.thresholdValue .grid(row=3, column=c+1, sticky=W)

        ################################################################################

        # Ground Truth button: check if the ground truth is considered
        self.var = IntVar(value=1)
        self.GTcheck = Checkbutton(master, text="Ground Truth", variable=self.var, state=ACTIVE, onvalue=1, offvalue=0)
        self.GTcheck.grid(row=4, column=c, columnspan=2)

        #################################################################################

        # Start calculation button
        self.startbutton = Button(master, text="start", command=self.start_calculation)
        self.startbutton.configure(font='bold 11 bold', width=6, height=3)
        self.startbutton.grid(row=5, column=c)
        
        # Quit button
        self.quitbutton = Button(master, text="QUIT", command=frame.quit)
        self.quitbutton.configure(fg="red", font='bold 11 bold', width=6, height=3)
        self.quitbutton.grid(row=5, column=c+1)

        #################################################################################

        # Set eposition and rposition     
        position = []
        self.predictedpositions = []
        self.gtpositions = []

        row = 0
        col = 0
        i = 1
        for pos in range(1,9):
            position.append( StringVar() )
            Label(master, text="Position %d"%i).grid(row=ro+row+1, column=col, sticky=E+N)

            self.predictedposition = Label(master,text="Predicted %d"%i)
            self.predictedposition.grid(row=ro+row+2, column=col, sticky=E+N, ipady=2, ipadx=2, rowspan=2)
            self.predictedposition.configure(width=9, height=5, bg="blue")
            self.predictedpositions.append(self.predictedposition)

            self.gtposition = Label(master,text="G. truth %d"%i)
            self.gtposition.grid(row=ro+row+2, column=col+1, sticky=W+N, ipady=2, ipadx=2, rowspan=2)
            self.gtposition.configure(width=9, height=5, bg="green")
            self.gtpositions.append(self.gtposition)

            col = col +2 
            i = i+1
            if i == 5:
                row = row +4
                col = 0

        #################################################################################

        # Make Legend
        Label(master, text="Legend").grid(row=ro+5, column=c, columnspan=2, sticky=S)
        
        predictedPositionLabel = Label(master, text="Predicted")
        predictedPositionLabel.configure(width=9, height=2, bg="blue")
        predictedPositionLabel.grid(row=ro+6, column=c, sticky=E+N)

        gtPositionLabel = Label(master, text="G. truth ")
        gtPositionLabel.configure(width=9, height=2, bg="green")
        gtPositionLabel.grid(row=ro+6, column=c+1, sticky=W+N)

        #################################################################################

        # Label Prediction
        self.scrollbar = Scrollbar(master)

        self.information = Label(master, text="Information", font='bold 10 bold')
        self.information.grid(row = ro, column = c, columnspan = 3)

        self.prediction = Text(master)
        self.prediction.configure( yscrollcommand=self.scrollbar.set, width=40, height=7)
        self.prediction.grid(row = ro+1, column = c, columnspan = 3, rowspan=3)

    def clean_position(self):
        # Set predictedpositions, rpositions and side information as in the beginning
        col = 0
        row = 0
        for i in range(8):
            self.gtpositions[i].grid_forget()
            self.predictedpositions[i].grid_forget()

            self.predictedpositions[i] = Label(root, text="Predicted %d"%(i+1))
            self.predictedpositions[i].grid(row=ro+row+2, column=col, sticky=E+N, ipady=2, ipadx=2)
            self.predictedpositions[i].configure(width=9, height=5, bg="blue")
            
            if(self.var.get()==1): # if ground truth is included
                self.gtpositions[i] = Label(root, text="G. truth %d"%(i+1))
                self.gtpositions[i].grid(row=ro+row+2, column=col+1, sticky=W+N, ipady=2, ipadx=2)
                self.gtpositions[i].configure(width=9, height=5, bg ="green")
            else:
                self.gtpositions[i].grid_forget()    

            col = col +2 
            if i == 3:
                row = row + 4 
                col = 0        

        self.prediction.delete('1.0', END) # clean information field

    def resize_image(self, image, newsize):
        # Resize image
        newimage = image.resize((newsize[0], newsize[1]), Image.ANTIALIAS)
        newimage = ImageTk.PhotoImage(newimage)
        return newimage

    def crop_image(self, image, cropsize ,newsize):
        # Crop the image and resize it
        newimage = image.crop(cropsize)
        newimage = self.resize_image(newimage, newsize)
        return newimage

    def resize_window(self):
        # Resize window to fit in any screen size
        for i in range(8):
            Grid.rowconfigure(root, i, weight=1)
            Grid.columnconfigure(root, i, weight=1)

    def open_image(self):
        # Choose an image
        self.prediction.config(state=NORMAL)
        self.prediction.delete('1.0', END) # clean information field
        self.imageoption = tkFileDialog.askopenfile(mode='rb', parent=root, title='Choose an image')

        if not isinstance(self.imageoption, NoneType):
            extension = os.path.splitext(self.imageoption.name)[1]
        else:
            if not isinstance(self.filedefault, NoneType):
                extension = os.path.splitext(self.filedefault)[1]
            else:
                extension = None

        if extension != '.png': # avoid error if the chosen file has a wrong extension
            print('Wrong extension')
            information = ("Choose a png file")
            self.prediction.insert(END, information)
        else:
            if not self.imageoption: #if cancel the option file use the last file
                if not isinstance(self.filedefault, NoneType): # avoid error if None
                    self.imagefile = Image.open(self.filedefault)
            else:
                self.imagefile = Image.open(self.imageoption)
                self.filedefault = self.imageoption.name #change the default to current file

        if not isinstance(self.filedefault, NoneType):# avoid error if None
            self.imagefile = cv2.imread(self.filedefault, 1)
            b, g, r = cv2.split(self.imagefile)
            self.imagefile = cv2.merge([r, g, b])

            self.newsize = (self.imagebutton.winfo_width(), self.imagebutton.winfo_height())#frame size
            self.im = Image.fromarray(self.imagefile)
            self.photo = self.resize_image(self.im, self.newsize)

            self.imagebutton = Button(root, text="Choose Image",
                                      command=lambda:
                                      [f() for f in [self.open_image,
                                                     self.clean_position,
                                                     self.resize_window]])
            self.imagebutton.config(image=self.photo)
            self.imagebutton.grid(row=0, column=0, columnspan=c, rowspan=ro)

        self.prediction.config(state=DISABLED)
        self.prediction.update()

    def center_box(self):
        # This function make a list with the center box of the ground truth json file
        # If there is no center box for some position, it returns None in the respective
        # position in the list.

        centerlist = [None]*8
        n = len(self.GTdata)

        for i in range (0, n):
            centerlist[self.GTdata[i]['iPosition'] - 1] = self.GTdata[i]['Center']

        return centerlist         

    def ground_truth(self, gt):
        # Update the widget gtpositions[x] with the respective image
        row = 0
        col = 0

        if gt:
            centerlist = self.center_box()
            x = 45
            y = 45
            self.ans =[]
            for i in range(8):
                p = centerlist[i]
                if isinstance(p, NoneType):
                    self.gtpositions[i].configure(image='', text="No icon", width=9,
                                                  height=5, bg ="green")
                    self.gtpositions[i].grid(row=ro+row+2, column=col+1, sticky=W+N,
                                             ipady=2, ipadx=2)
                else:
                    ansTemp  = self.crop_image(self.im, (p[0]-x, p[1]-y, p[0]+x,p[1]+y), (80, 80))
                    self.ans.append(ansTemp)
                    self.gtpositions[i].configure(image=self.ans[i], width=80, height=80)
                    self.gtpositions[i].grid(row=ro+row+2, column=col+1, sticky=W+N,
                                             ipady=2, ipadx=2)
                col = col +2 
                if i == 3:
                    row = row +4 
                    col = 0 
        else:
            for i in range(8):
                self.gtpositions[i].configure(image='',width=9, height=5, bg="black")
                self.gtpositions[i].grid(row=ro+row+2, column=col+1, sticky=W+N,
                                         ipady=3, ipadx=3)   
                col = col +2 
                if i == 3:
                    row = row +4
                    col = 0 

    def no_ground_truth(self):
        # If there is no ground truth, remove the widget gtpositions[x] from the window
        for i in range(0, 8):
            self.gtpositions[i].grid_forget()

    def predicted_position(self):
        # Update the widget predictedpositions[x] with the respective image
        results = self.data

        row = 0
        col = 0
        self.anse =[]

        for i in range(8):
            if isinstance(results['iPosition%d'%(i+1)], NoneType):
                self.predictedpositions[i].configure(image='', text="No position \n predicted",
                                                     width=9, height=5, bg="blue")
                self.predictedpositions[i].grid(row = ro+row+2, column = col, sticky=E+N,ipady=2, ipadx=2)
            else:
                anseTemp = self.crop_image(self.im, results['iPosition%d'%(i+1)]['Box'], (80, 80))
                self.anse.append(anseTemp)
                self.predictedpositions[i].configure(image=self.anse[i], width=80, height=80)
                self.predictedpositions[i].grid(row=ro+row+2, column=col, sticky=E+N,
                                                ipady=2, ipadx=2)
            col = col +2 
            if i == 3:
                row = row +4
                col = 0 

    def print_information(self):
        # Print additional information about the calculation and the result positions

        Results = self.data
        score = []
        iconLabel = [] 

        if self.var.get()  == 1:
            gt = "Ground truth selected"
        else:
            gt = "Ground truth not selected"

        row = 0
        col = 0
        for i in range(8):
            if not isinstance(Results['iPosition%d'%(i+1)],NoneType):
                score.append('Position %d score: %.5f\n'%((i+1), Results['iPosition%d'%(i+1)]['Score']))
                iconLabel.append('Position %d label:%s\n'%((i+1), Results['iPosition%d'%(i+1)]['IconLabel']))

        information = (
            "File name: %s\n\n"%self.filedefault +
            "Prediction: %s\n"%self.predictionValue.get() +
            "Keep: %s\n"%self.keepValue.get() +
            "Threshold: %s\n"%self.thresholdValue.get() +
            "Network: %s\n"%self.variable.get() +
            "%s\n\n"%gt +
            "%s"%''.join(map(str, score)) + "\n"+
            "%s"%''.join(map(str, iconLabel)))

        self.prediction.config(state=NORMAL)
        self.prediction.delete('1.0', END) # clean information field
        self.prediction.insert(END, information)

    def start_calculation(self):

        if isinstance(self.filedefault, NoneType):
            print("No image loaded")
            information = ("Load an image")
            self.prediction.delete('1.0', END) # clean information field
            self.prediction.insert(END, information)
        else:
            print("start calculation")	

            net = self.variable.get()
            keep = self.keepValue.get()
            thresh = self.thresholdValue.get()
            pred = self.predictionValue.get()
            img = self.filedefault

            # open json file of the ground truth
            jsonname = img.split(".png")[0]+".json"
            if os.path.isfile(jsonname):
                gt = True
                with open(jsonname) as data_file:    
                    self.GTdata = json.load(data_file)
            else:
                gt = False

            if 1:
                self.data = run_faster_rcnn(net, img, cpu_mode=0)
            
            if self.var.get() == 1:
                self.ground_truth(gt)
                self.predicted_position()
            else:
                self.no_ground_truth()  
                self.predicted_position()  
            self.print_information()

def run_faster_rcnn(net, img, cpu_mode=0):
    ''' This is a demo function for you to get equal results as my function will deliver!'''
    import pprint
    import time

    class ownTimer(object):
        def __init__(self, name=None):
            self.name = name

        def __enter__(self):
            self.tstart = time.time()

        def __exit__(self, exc_type, exc_val, exc_tb):
            if self.name:
                print("[%s]" % self.name),
            print(" %.6f Seconds" % (time.time() - self.tstart))

    def Intersection_over_Union(box, gt_box):
        x1, y1, x2, y2 = box
        gtx1, gty1, gtx2, gty2 = gt_box

        # determine the (x, y)-coordinates of the intersection rectangle
        xA = max(x1, gtx1)
        yA = max(y1, gty1)
        xB = min(x2, gtx2)
        yB = min(y2, gty2)

        # compute the area of intersection rectangle
        if xB - xA < 0 or yB - yA < 0:
            return 0

        interArea = (xB - xA + 1) * (yB - yA + 1)

        # compute the area of both the prediction and ground-truth
        # rectangles
        boxArea = (x2 - x1 + 1) * (y2 - y1 + 1)
        boxGTArea = (gtx2 - gtx1 + 1) * (gty2 - gty1 + 1)

        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the interesection area
        iou = interArea / float(boxArea + boxGTArea - interArea)
        return iou

    def getCenterFromBox(coordinates):
        x1, y1, x2, y2 = coordinates
        center_x = round(float((x1 + x2) / 2))
        center_y = round(float((y1 + y2) / 2))
        return (int(center_x), int(center_y))

    def gt_box_for_iPosition(i):
        context = "Media"
        # x1, y1, x2, y2
        if i == None: return None
        if i == 1:
            gt_box = (320, 264, 400, 344)
        if i == 2:
            gt_box = (640, 264, 720, 344)
        if i == 3:
            gt_box = (960, 264, 1040, 344)
        if i == 4:
            gt_box = (1280, 264, 1360, 344)
        if i == 5:
            gt_box = (320, 470, 400, 550)
        if i == 6:
            gt_box = (640, 470, 720, 550)
        if i == 7:
            gt_box = (960, 470, 1040, 550)
        if i == 8:
            gt_box = (1280, 470, 1360, 550)
        return gt_box

    CLASSES = ('__background__',  # always index 0
               'icon_sourceselection_bt', 'icon_sourceselection_bt_not_connected',  # 1, 2
               'icon_sourceselection_bt_selected', 'icon_sourceselection_changer1',  # 3, 4
               'icon_sourceselection_changer1_greyed_out', 'icon_sourceselection_changer1_selected',  # 5, 6
               'icon_sourceselection_changer2', 'icon_sourceselection_changer2_greyed_out',  # 7, 8
               'icon_sourceselection_changer2_selected', 'icon_sourceselection_changer3',  # 9, 10
               'icon_sourceselection_changer3_greyed_out', 'icon_sourceselection_changer3_selected',  # 11, 12
               'icon_sourceselection_changer4', 'icon_sourceselection_changer4_greyed_out',  # 13, 14
               'icon_sourceselection_changer4_selected', 'icon_sourceselection_changer5',  # 15, 16
               'icon_sourceselection_changer5_greyed_out', 'icon_sourceselection_changer5_selected',  # 17, 18
               'icon_sourceselection_changer6', 'icon_sourceselection_changer6_greyed_out',  # 19, 20
               'icon_sourceselection_changer6_selected', 'icon_sourceselection_changer_greyed_out',  # 21, 22
               'icon_sourceselection_internal', 'icon_sourceselection_internal_greyed_out',  # 23, 24
               'icon_sourceselection_internal_selected', 'icon_sourceselection_ipod1',  # 25, 26
               'icon_sourceselection_ipod1_selected', 'icon_sourceselection_ipod2',  # 27, 28
               'icon_sourceselection_ipod2_selected', 'icon_sourceselection_ipod3',  # 29, 30
               'icon_sourceselection_ipod3_selected', 'icon_sourceselection_ipod4',  # 31, 32
               'icon_sourceselection_ipod4_selected', 'icon_sourceselection_onlinemusic',  # 33, 34
               'icon_sourceselection_sd1', 'icon_sourceselection_sd1_greyed_out',  # 35, 36
               'icon_sourceselection_sd1_selected', 'icon_sourceselection_tv',  # 37, 38
               'icon_sourceselection_tv_greyed_out', 'icon_sourceselection_tv_selected',  # 39, 40
               'icon_sourceselection_usb1', 'icon_sourceselection_usb1_greyed_out',  # 41, 42
               'icon_sourceselection_usb1_selected', 'icon_sourceselection_usb2',  # 43, 44
               'icon_sourceselection_usb2_selected', 'icon_sourceselection_usb3',  # 45, 46
               'icon_sourceselection_usb3_selected', 'icon_sourceselection_usb4',  # 47, 48
               'icon_sourceselection_usb4_selected', 'icon_sourceselection_usb_greyed_out')  # 49, 50

    print("Used Net %s" % net)
    output = img.split('.')[0] + '_results.pkl'
    if os.path.isfile(output):
        data = np.load(output)
        scores = data['scores']
        boxes = data['boxes']

    scores_wo_bg = []

    for score in scores:
        scores_wo_bg.append(np.max(score[1:]))
    maxs_idx_sorted = sorted(range(len(scores_wo_bg)), key=lambda k: scores_wo_bg[k])

    context = "Media"
    threshold = 0.4
    maxs_idx_sorted.reverse()
    DAS_Frames = {}
    if 1:
        detectedIcons = []
        trueIcons = 0
        counter = 0
        with ownTimer("Allocate Detections"):
            # for idx, label in enumerate(CLASSES):
            for idx in maxs_idx_sorted:
                # if idx == 0: continue  # no background
                # for pos, score in enumerate(scores[:, idx]):
                score = np.max(scores_wo_bg[idx])
                if score > threshold:
                    try:
                        counter += 1
                        detectedIcon = {}
                        detectedIcon["Box"] = boxes[idx][
                                              (np.argmax(scores_wo_bg[idx]) + 1) * 4:(np.argmax(
                                                  scores_wo_bg[idx]) + 1) * 4 + 4]
                        detectedIcon["Center"] = getCenterFromBox(detectedIcon["Box"])
                        detectedIcon["IconNr"] = np.argmax(scores[idx][1:]) + 1
                        detectedIcon["IconLabel"] = CLASSES[detectedIcon["IconNr"]][21:]
                        detectedIcon["Score"] = score
                        # detectedIcon["iPosition"] = getiPosition(detectedIcon["Center"], context)
                        max_iou = 0
                        detectedIcon["iPosition"] = None
                        for i in range(1, 9):
                            iou = Intersection_over_Union(detectedIcon["Box"], gt_box_for_iPosition(i))
                            if iou > max_iou:
                                max_iou = iou
                                if max_iou > 0.25:
                                    detectedIcon["iPosition"] = i
                        if detectedIcon["iPosition"] == None: continue
                        detectedIcon["IoU"] = max_iou
                        detectedIcons.append(detectedIcon)
                        # pprint(detectedIcon)
                    except:
                        pass
        detectedSources = {}
        filename = "Media"  ##################
        if "Radio" in filename:
            context = "Radio"
            iPositions = 6
        if "Media" in filename:
            context = "Media"
            iPositions = 8
        for iPos in range(1, 9):
            max_score = 0
            detectedSources["iPosition%s" % iPos] = None
            for detectedIcon in detectedIcons:
                if detectedIcon["iPosition"] == iPos and max_score < detectedIcon["Score"]:
                    max_score = detectedIcon["Score"]
                    detectedSources["iPosition%s" % iPos] = detectedIcon

    return detectedSources

root = Tk()

app = App(root)
app.resize_window()

root.mainloop()
root.destroy()